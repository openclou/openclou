function
dialog_hire_default()
	entry = {
		{"Interessiert an Arbeit?", callback_need_work()},
		{"Schon Erfahrung", callback_experience()},
		{"Schon was mit den Bullen zu tun gehabt?", callback_cops},
		{"Tschuessi.", callback_end()}
	}

	return entry
end

function
build_xp_string( _xp )

	local string

	if _xp.open_saves() > 90 then
		string = string .. "Ich bin Experte im Safes knacken."
	elseif _xp.open_saves() > 30 then
		string = string .. "Ich kenn mich mit Safes ganz gut aus."
	end

	if _xp.electronics() > 90 then
		string = string .. "Kein Schloss ist vor mir sicher."
	elseif _xp.electronics() > 30 then
		string = string .. "Ich weiss wie ein Dietrich aussieht."
	end

	return string
end

function
callback_experience()
	dialog.Says(build_xp_string(game::current_dialog::npc.get_experience_stuff()));
end

function
callback_cops()
	local facts = game::current_dialog::npc.get_police_facts()
	local string

	if facts > 90 then
		string = "Ich bin Staatsfeind Nr. 1"
	else if facts > 80 then
		string = "Die Bullen haengen mir staengig am Arsch"
	else if facts > ... then
		...
	end

	return string
end

function
callback_need_work()

	dialog.MultipleChoice(
	{
		{"Wir sind zu 2.", callback_need_work_answer(1)},
		{"Wir sind zu 3.", callback_need_work_answer(2)},
		{"Wir sind zu 4.", callback_need_work_answer(3)}
	})
end

function
callback_need_work_answer( _answer )
	local eddi = game::current_dialog::npc
	local percent = eddi.get_percent_bla(_answer)
	
	dialog.Says("Dann will ich " .. percent .. "% von der Beute.");

	dialog.MultipleChoice(
	{
		{"Geht klar.", callback_percent_answer(0)},
		{"Das ist mir zu viel.", callback_percent_answer(1)},
	})
end

function
callback_percent_answer( _answer )
	if 0 == _answer then
		game::npc("matt").add_to_accomplice_list(dialog::current_dialog::npc)
	end
end

function
callback_end_dialog()
	dialog.run = false
end
