OpenClou!
=========

Getting started
---------------

Download the latest dialogic release from https://github.com/coppolaemilio/dialogic/releases and extract it to addons (tested with 1.4.1 on Godot 3.4.4). Afterwards enable the plugin in the project settings.