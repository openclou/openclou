extends CharacterBody3D

var character

const SPEED = 5.0
const ACCELERATION = 3
const DE_ACCELERATION = 5
const JUMP_VELOCITY = 4.5

@export var cash = 0.0
signal cash_in_wallet_changed(new_value: float)

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	character = get_node(".")

func _physics_process(delta):

	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
		return

	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("ui_jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Obtain current camera position
	var camera = get_node("target/Camera2").get_global_transform()

	var dir = Vector3()
	var is_moving = false

	# Identify intended action of the user
	if (Input.is_action_pressed("ui_up")):
		dir += -camera.basis[2]
		is_moving = true
	if (Input.is_action_pressed("ui_down")):
		dir += camera.basis[2]
		is_moving = true
	if (Input.is_action_pressed("ui_left")):
		dir += -camera.basis[0]
		is_moving = true
	if (Input.is_action_pressed("ui_right")):
		dir += camera.basis[0]
		is_moving = true

	# Y is handled by gravity, ignore camera for this
	dir.y = 0
	dir = dir.normalized()

	var hv = velocity
	hv.y = 0

	var new_pos = dir * SPEED
	var accel = DE_ACCELERATION
	
	if (dir.dot(hv) > 0):
		accel = ACCELERATION

	hv = hv.lerp(new_pos, accel * delta)

	velocity.x = hv.x
	velocity.z = hv.z

	set_up_direction(Vector3(0,1,0))
	move_and_slide()

	if dir:
		var angle = atan2(hv.x, hv.z)

		var char_rot = character.get_rotation()

		char_rot.y = angle
		character.set_rotation(char_rot)


func _on_area_3d_area_entered(area):
	if area.get_parent().is_in_group("valuables"):
		# Found a collectable item, grab it
		cash += area.get_parent().value
		
		$PickupSound.position = area.get_parent().position
		$PickupSound.play()
		
		area.get_parent().queue_free()
		print("Sending cash", str(cash))
		emit_signal("cash_in_wallet_changed", cash)
