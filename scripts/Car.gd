extends Node
class_name Car

# Number of seats in the car
@export var seats: int = 2
# Horse power of the car
@export var hp: int = 45
# Maximum speed of the car in km/h
@export var max_speed: int = 50
# Monetary value when buying the car
@export var value: int = 500
