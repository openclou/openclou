extends Control

func _ready():
	var player = get_tree().get_current_scene().find_child("Player", true, false)
	player.cash_in_wallet_changed.connect(_on_cash_in_wallet_changed)

func _on_cash_in_wallet_changed(new_value):
	print("Receiving cash", str(new_value))
	$bottom_bg/TextureRect/Label.text = "$ " + str(new_value)
